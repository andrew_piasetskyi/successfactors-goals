import { Post } from "../types";

export interface PostsState {
  ids: string[];
  all: Record<string, Post>;
  loaded: boolean;
}

export const initialPostsState = (): PostsState => {
  return {
    ids: [],
    all: {},
    loaded: false
  };
};
