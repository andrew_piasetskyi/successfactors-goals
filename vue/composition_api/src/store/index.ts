import { reactive, readonly } from "vue";

import { PostsState, initialPostsState } from "./timeline";

interface State {
  posts: PostsState;
}

/*imported modules states*/
const initialState = () => {
  return {
    posts: initialPostsState()
  };
};

class Store {
  protected state: State;

  constructor(initialState: State) {
    this.state = reactive(initialState);
  }

  public getState(): State {
    return readonly(this.state);
  }
}

const store = new Store(initialState());
store.getState();
