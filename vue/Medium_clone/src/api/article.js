import axios from '@/api/axios'

const getArticle = slug => {
    return axios.get(`/articles/${slug}`).then(response => response.data.article)
}

const deleteArticle = slug => {
    return axios.delete(`/articles/${slug}`)
}

const createArticle = articleData => {
    return axios.post('/articles', {article: articleData})
        .then(response => response.data.article)
}

const editArticle = (slug, articleData) => {
    return axios
        .put(`/articles/${slug}`, articleData )
        .then(response => response.data.article)
}

export default {
    getArticle,
    deleteArticle,
    createArticle,
    editArticle
}