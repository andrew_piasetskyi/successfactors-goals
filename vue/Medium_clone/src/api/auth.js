import axios from '@/api/axios'

const register = credentials => {
    return axios.post('/users', {user:credentials})
}

const login = credentials => {
    return axios.post('/users/login', {user:credentials})
}

const getCurrentUser = () => {
    return axios.get('/user')
}

const updateCurrentUser = currentUserData => {
    return axios.put('/user', {user: currentUserData})
        .then(response => response.data.user)
}

export default {
    register,
    login,
    getCurrentUser,
    updateCurrentUser
}

/*
test credentials
    Sleivin047
    sleivin047@mail.com
*/