import { createRouter, createWebHashHistory } from 'vue-router';
import CGlobalFeed from '@/views/GlobalFeed';
import CYourFeed from '@/views/YourFeed';
import CTagFeed from '@/views/TagFeed';
import CRegister from '@/views/Register';
import CLogin from '@/views/Login';
import CArticle from '@/views/Article';
import CCreateArticle from '@/views/CreateArticle';
import CEditArticle from '@/views/EditArticle';
import CSettings from '@/views/Settings';
import CUserProfile from '@/views/UserProfile'

const routes = [
  {
    path: '/register',
    name: 'register',
    component: CRegister
  },
  {
    path: '/login',
    name: 'login',
    component: CLogin
  },
  {
    path: '/',
    name: 'globalFeed',
    component: CGlobalFeed
  },
  {
    path: '/feed',
    name: 'yourFeed',
    component: CYourFeed
  },
  {
    path: '/tags/:slug?',
    name: 'tag',
    component: CTagFeed
  },
  {
    path: '/articles/:slug?',
    name: 'article',
    component: CArticle
  },
  {
    path: '/articles/new',
    name: 'createArticle',
    component: CCreateArticle
  },
  {
    path: '/articles/:slug?',
    name: 'editArticle',
    component: CEditArticle
  },
  {
    path: '/settings',
    name: 'settings',
    component: CSettings
  },
  {
    path: '/profiles/:slug?',
    name: 'userProfile',
    component: CUserProfile
  },
  {
    path: '/profiles/:slug?/favorites',
    name: 'userProfileFavorites',
    component: CUserProfile
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
