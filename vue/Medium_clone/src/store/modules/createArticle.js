import articleApi from '@/api/article'

const state = {
    isSubmitting: false,
    validation: null
}

export const mutationTypes = {
    createArticleStart: '[createArticle] Create article start',
    createArticleSuccess: '[createArticle] Create article success',
    createArticleFailure: '[createArticle] Create article failure'
}

export const actionTypes = {
    createArticle: '[createArticle] create article'
}

const mutations = {
    [mutationTypes.createArticleStart](state) {
        state.isSubmitting = true
    },
    [mutationTypes.createArticleSuccess](state) {
        state.isSubmitting = false 
    },
    [mutationTypes.createArticleFailure](state, payload) {
        state.isSubmitting = false
        state.validationErrors = payload
    }
}

const actions = {
    [actionTypes.createArticle](context, {articleData}) {
        return new Promise(resolve => {
            context.commit(mutationTypes.createArticleStart)
            articleApi.createArticle(articleData)
            .then(article => {
                context.commit(mutationTypes.createArticleStart, article)
                resolve(article)
            }).catch(result => {
                context.commit(mutationTypes.createArticleFailure, result.response.data.errors )
            })
        })
    }
}

export default {
    state,
    actions,
    mutations
}