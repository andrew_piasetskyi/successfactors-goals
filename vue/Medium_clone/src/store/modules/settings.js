import {mutationTypes as authMutaionTypes} from '@/store/modules/auth'

const state = {
    isSubmiting: false,
    validationErrors: null
}

const mutations = {
    [authMutaionTypes.updateCurrentUserStart](state) {
        state.isSubmiting = true
        state.validationErrors = null
    },
    [authMutaionTypes.updateCurrentUserSuccess](state) {
        state.isSubmiting = false
    },
    [authMutaionTypes.updateCurrentUserFailure](state, payload) {
        state.isSubmiting = false
        state.validationErrors = payload
    }
}

export default {
    state,
    mutations
}