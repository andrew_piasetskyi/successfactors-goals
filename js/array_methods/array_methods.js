/* Array.of() - create array from keys */

const arrOf = Array.of('john', 'dou');
console.log(arrOf); //["john", "dou"]

const parseStr = [...'wes'];
console.log(parseStr); //["w", "e", "s"]



/* Array.from() - create array from object */

const arrFrom = Array.from({ length: 10 }, function() {
    return 'value';
});
console.log(arrFrom) //["value", "value", "value", "value", "value", "value", "value", "value", "value", "value"]

function createRange (start, end) {
    const range = Array.from({length: end - start + 1},
        function(item, index) {
            return index + start;
        });
        return range;
}
console.log(createRange(3,7)); //[3, 4, 5, 6, 7]

const listItems = document.querySelectorAll('.li');
Array.from(listItems).map((item) => { // way to iterate the HTML collection
    console.log(item.innerText);
})



/* Array.isArray() - check is array */

const arr = [1,2,30];
console.log(Array.isArray(arr)) // true



/* Object.entries(), Object.keys(), Object.values() */

someObj = {name: 'john', mail: 'dou', password: '123' }
console.log(Object.entries(someObj)); // [['name', 'john'], ['mail', 'dou'], ['password', '123']]
console.log(Object.keys(someObj)); // ["name", "mail", "password"]
console.log(Object.values(someObj)); // ["john", "dou", "123"]



/* Array.join() turn array into a string*/

const cars = ['bmw', "audi", "mersedes"];
console.log(cars.join()); // 'bmw,audi,mersedes'



/* Array.slice() returns new array existing a part of initial array*/
/* Array.splice() mutates initial array geting a part of it*/

const numbers = [1, 2, 3, 4, 5, 6, 7];
const numbersSlice = numbers.slice(1,5);
console.log(numbersSlice); // [2, 3, 4, 5]



/* Array.push() adds new element in to initial array */

const mtb = ['ns', 'dartmoor', 'commencal'];
const pushMtb = mtb.push('spcialized');
console.log(mtb); // ["ns", "dartmoor", "commencal", "spcialized"]



/* Push new element into middle of array */

const deCars = ['vw', 'audi', 'mersedes']
const updatedDeCars = [...deCars.slice(0, 2), 'bmw', ...deCars.slice(2)];
console.log(updatedDeCars); // ["vw", "audi", "bmw", "mersedes"]



/* Array.indexOf() method returns the first index by the given element 
can be found in the array or -1 if there is no such index. */

const users = ['Andrew', 'Olga', 'Serjio', 'Natalia']
console.log(users.indexOf('Serjio')); // 2
console.log(users.indexOf('Dima')); // -1



/* To prevent mutation of original array take a copy of the array */

const originalArray = [1, 2, 3];
const copy = [...originalArray];
console.log(copy); // [1, 2, 3]



/* Array.forEach() once invokes function for every array element */
/* Also applicible for NodeList */

const saesons = ['winter', 'spring', 'summer', 'autumn'];
saesons.forEach(saeson => console.log(saeson)); // winter
                                                // spring
                                                // summer
                                                // autumn



/* Array.map runs callback for each value in the array and returns
each new value in the resulting array */                                                

const officers = [
    { id: 20, name: 'Captain Piett' },
    { id: 24, name: 'General Veers' },
    { id: 56, name: 'Admiral Ozzel' },
    { id: 88, name: 'Commander Jerjerrod' }
  ];

const officersIds = officers.map(officer => officer.id);
console.log(officersIds); // [20, 24, 56, 88]



/* Array.reduce() runs callback for each value in the array and 
passes the result of this callback (the accumulator) from one array element to the other */    

var pilots = [
    {
      id: 10,
      name: "Poe Dameron",
      years: 14,
    },
    {
      id: 2,
      name: "Temmin 'Snap' Wexley",
      years: 30,
    },
    {
      id: 41,
      name: "Tallissan Lintra",
      years: 16,
    }
  ];

  const totalYears = pilots.reduce((acc, pilot) => acc + pilot.years, 0);
  console.log(totalYears); // 60



  /* Array.filter() creates a new array of elements that have passed 
  the condition in the callback function */

  const saleItems = [
    {
      id: 2,
      name: "boots",
      gender: "mens",
    },
    {
      id: 8,
      name: "pants",
      gender: "women's",
    },
    {
      id: 40,
      name: "boots",
      gender: "women's",
    },
    {
      id: 66,
      name: "shoes",
      gender: "mens",
    }
  ];

  const mensItems = saleItems.filter(item => item.gender === "mens");
  console.log(mensItems); // [
                          //    {id: 2, name: "boots", gender: "mens"},
                          //    {id: 66, name: "shoes", gender: "mens"}
                          // ]



/* List of array methods that mutate initial array:*/

// copyWithin
// fill
// pop
// push
// reverse
// shift
// sort
// splice
// unshift
