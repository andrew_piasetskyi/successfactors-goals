/* ES6 */
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }

    get area() {
        return this.height * this.width;
    }
}



class  Square extends Rectangle {
    constructor (side) {
        super(side, side)
    }

    get side() {
        return this.height;
    }
}

/* ES5 */

function Rect(height, width) {
    this.height = height;
    this.width = width;
}

Rect.prototype.area = function () {
    return this.height * this.width;
}

function Sqr(height, width, side) {
    Rect.call(this, height, width, side)
}

Object.setPrototypeOf(Sqr.prototype, Rect.prototype);