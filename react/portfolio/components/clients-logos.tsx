import React from 'react';
import Image from 'next/image';
import SliderContainer, {SliderItem} from './slider';

const CLientLogos: React.FC = () => (
    <>
        <SliderContainer contentWidth={1290} initialOffsetX={0}>
            <SliderItem width={150}>
                <Image 
                    src="/assets/trustedby/audobon.webp"
                    width={150}
                    height={50}
                    alt="auduban"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/pinkpanda.webp"
                    width={150}
                    height={50}
                    alt="pinkpanda"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image 
                    src="/assets/trustedby/exodus.webp"
                    width={150}
                    height={50}
                    alt="exodus"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/expensify.webp"
                    width={150}
                    height={50}
                    alt="expensify"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/extra.webp"
                    width={150}
                    height={50}
                    alt="extra"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/litentry.webp"
                    width={150}
                    height={50}
                    alt="litentry"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/nativescript.webp"
                    width={150}
                    height={50}
                    alt="nativescript"
                    objectFit="contain"
                />
            </SliderItem>
           
        </SliderContainer>
        <SliderContainer contentWidth={1290} initialOffsetX={0}>            
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/coinbase.webp"
                    width={150}
                    height={50}
                    alt="coinbase"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/rainbow.webp"
                    width={150}
                    height={50}
                    alt="rainbow"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/scribeware.webp"
                    width={150}
                    height={50}
                    alt="scribeware"
                    objectFit="contain"
                />
            </SliderItem>
            <SliderItem width={150}>
                <Image
                    src="/assets/trustedby/picnic.webp"
                    width={150}
                    height={50}
                    alt="picnic"
                    objectFit="contain"
                />
            </SliderItem>
        </SliderContainer>
    </>
)

export default CLientLogos;