import React from 'react';
import { Tile, TileBackground, TileContent, TileWrapper } from './tile';
import { spawn } from 'child_process';
import { WorkBackground, WorkContainer, WorkLeft, WorkRight, WorkLink } from './work';
import Image from 'next/image';

const Works = () => <TileWrapper numOfPages = {3}>
    <TileBackground>
        <WorkBackground />
    </TileBackground>
    <TileContent>
        <Tile 
            page={0} 
            renderContent={({progress}) => (
                <WorkContainer>
                    <WorkLeft progress={progress}>
                        <div>We built</div>
                        <div className="text-4xl md:text-5xl font-semibold tracking-tight"> 
                            <WorkLink href="https://pinkpanda.io">Pink Panda&apos;s app </WorkLink>
                        </div>
                    </WorkLeft>
                    <WorkRight progress={progress}>
                        <Image 
                            src="/assets/works/pinkpanda.webp"
                            layout="responsive"
                            width={840}
                            height={1620}
                            alt="Pink panda" 
                        />
                    </WorkRight>
                </WorkContainer>)}
        ></Tile>
    </TileContent>
    <TileContent>
        <Tile
            page={1}
            renderContent={({ progress }) => (<WorkContainer>
                <WorkLeft progress={progress}>
                    <div>We made</div>
                    <div className="text-4xl md:text-5xl font-semibold tracking-tight">
                        <WorkLink href="https://coinbase.com">Coinbase Wallet faster.</WorkLink>
                    </div>
                </WorkLeft>
                <WorkRight progress={progress}>
                    <Image
                        src="/assets/works/coinbase_wallet.jpg"
                        layout="responsive"
                        width={840}
                        height={1620}
                        alt="Coinbase Wallet"
                    />
                </WorkRight>
            </WorkContainer>)}
        ></Tile>
    </TileContent>
    <TileContent>
        <Tile
            page={2}
            renderContent={({ progress }) => (<WorkContainer>
                <WorkLeft progress={progress}>
                    <div>We helped</div>
                    <div className="text-4xl md:text-5xl font-semibold tracking-tight">
                        <WorkLink href="https://showtime.xyz/">Showtime ship faster</WorkLink>
                    </div>
                </WorkLeft>
                <WorkRight progress={progress}>
                    <Image
                        src="/assets/works/showtime.webp"
                        layout="responsive"
                        width={840}
                        height={1620}
                        alt="Showtime"
                    />
                </WorkRight>
                <span className="text-9xl">Foo {progress}</span>
            </WorkContainer>)}
        ></Tile>
    </TileContent>
</TileWrapper>

export default Works